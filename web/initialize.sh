#log permission
mkdir -p -m=775 storage/logs

# laravel folders
mkdir -p -m=777 storage/framework/cache/data
mkdir -p -m=777 storage/framework/sessions
mkdir -p -m=777 storage/framework/views

# Permission
chown -R 33:33 ./

# composer
composer install
composer dump-autoload

# npm
# in case of npm ERR!, need execute "npm install -g npm"
# npm install -g npm
npm install
npm run dev

# migrate
# php artisan migrate:fresh

# seed
# php artisan db:seed

# npm run watch