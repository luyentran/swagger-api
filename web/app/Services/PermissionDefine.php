<?php
namespace App\Services;

class PermissionDefine
{
    const CMS_GUARD = 'admin';
    /**
     * Bắt buộc khai báo permission trong đây để db seed.
     *
     * @return array
     */
    public static function getPermissions() {
        $permissions = [];
        $routes = app('router')->getRoutes();
        foreach ($routes as $route) {
            if (!self::isCMSRoute($route)) continue;
            array_push($permissions, self::generatePermissionByRoute($route));
        }

        return $permissions;
    }

    private static function isCMSRoute($route)
    {
        return (isset($route['action']['middleware']) && in_array('auth:'.self::CMS_GUARD, $route['action']['middleware']));
    }

    public static function generatePermissionByRoute($route)
    {
        $name = self::generatePermissionNameByRoute($route);
        return [
            'guard_name' => self::CMS_GUARD,
            'method' => $route['method'],
            'route' => $route['uri'],
            'name' => $name
        ];
    }

    private static function generatePermissionNameByRoute($route)
    {
        $parts = explode('\\', $route['action']['uses']);
        $controllerAndMethodPart = end($parts);
        $controllerName = explode('Controller', $controllerAndMethodPart)[0];
        $functionName = explode('@', $controllerAndMethodPart)[1];

        return self::fromCamelCaseToSnakeCase($controllerName.'_'.$functionName);
    }

    private static function fromCamelCaseToSnakeCase($inputString) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $inputString, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
