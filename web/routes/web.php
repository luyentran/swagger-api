<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'app'], function ($router) {
    $router->get('/', 'IndexController@index');
    $router->post('login', 'AuthController@login');
    $router->group(['middleware' => ['auth:admin', 'scopes:cms-scopes']], function($router) {
        $router->get('logout', 'AuthController@logout');
        $router->get('user', 'AuthController@user');
    });
});
$router->group(['middleware' => ['auth:admin', 'scopes:cms-scopes']], function($router) {
    $router->group(['prefix' => 'users'], function($router) {
        $router->get('/', 'UserController@index');
        $router->get('{id}', 'UserController@show');
        $router->put('', 'UserController@selfUpdate');
        $router->put('{id}', 'UserController@update');
        $router->post('/', 'UserController@store');
        $router->post('password', 'UserController@selfPassword');
        $router->delete('/{id}', 'UserController@destroy');
    });
});
