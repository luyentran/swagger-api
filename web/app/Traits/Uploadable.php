<?php

namespace App\Traits;


use App\Services\Upload;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

/**
 * Trait Uploadable
 * @package App\Traits
 */
trait Uploadable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function upload(){
        $this->validate(request(),[
            'files.*'=>'required|max:10240',
        ]);
        $files = request()->file('files');
        if(!$files || empty($files)) {
            abort(400, 'No files upload');
        }
        $data = [];
        $uploadService = new Upload();
        foreach($files as $file){
            $data[] = $uploadService->singleUpload($file);
        }
        return $this->success($data);
    }

    public function uploadOrder(){
        $this->validate(request(),[
            'files.*'=>'required|max:10240',
        ]);
        $files = request()->file('files');
        if(!$files || empty($files)) {
            abort(400, 'No files upload');
        }
        $data = [];
        $uploadService = new Upload();
        foreach($files as $file){
            $data[] = $uploadService->singleUpload($file);
        }
        return $data;
    }
}
