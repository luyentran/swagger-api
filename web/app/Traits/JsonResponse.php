<?php
namespace App\Traits;


trait JsonResponse
{
    /**
     * Success Response
     *
     * @param $data
     * @param string $message
     * @param array $otherInfo
     * @param int $code
     * @param array $headers
     * @param int $options
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($data=null, $message = "", $otherInfo = [], $code = 200, $headers = [], $options = 0){
        /**
         * @OA\Schema(
         *   schema="JsonResponse",
         *   @OA\Property(
         *     property="status",
         *     type="string",
         *     description="Status message, success or error",
         *     example="400"
         *  ),
         *   @OA\Property(
         *     property="message",
         *     type="string",
         *     description="Detail message",
         *     example="message content"
         *  ),
         *   @OA\Property(
         *     property="data",
         *     type="object",
         *     description="Data of response",
         *  )
         *
         * )
         */
        $response = [
            'status' => 'success',
            'message' => $message,
            'data' => $data
        ];

        return response()->json(array_merge($response, $otherInfo), $code, $headers, $options);
    }

    /**
     *  Error Response
     * @param string $message
     * @param array $otherInfo
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function error($message = '', $otherInfo = [], $code = 400) {
        $response = [
            'status' => 'error',
            'message' => $message,
            'data' => null
        ];
        return response()->json(array_merge($response, $otherInfo), $code);
    }
}
