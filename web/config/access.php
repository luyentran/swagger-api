<?php

return [
    /*
     * Table names for access tables
     */
    'table_names' => [
        'users' => 'users',
    ],

    /*
     * Configurations for the user
     */
    'users' => [
        /*
         * The name of the super administrator role
         */
        'master_admin_role' => 'master_administrator',

        /*
         * The name of the super administrator role
         */
        'admin_role' => 'administrator',

        /*
        * The name of the manager role
        */
        'manager_role' => 'manager',

        /*
         * The name of the accountant role
         */
        'accountant_role' => 'accountant',

        /*
         *
         * The name of the storekeeper role
         */
        'storekeeper_role' => 'storekeeper',

        /*
         *
         * The name of the business role
         */
        'business_role' => 'business',

        /*
         * The default role all new registered users get added to
         */
        'default_role' => 'user',

        'accounts_cannot_editable' => array(1),

        'agency_level_master' => [1,2,3]
    ]
];
