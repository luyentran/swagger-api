<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\JsonResponse;
use App\Traits\JsonResponse as AppJsonResponse;

class Handler extends ExceptionHandler
{
    use AppJsonResponse;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @throws Exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function render($request, Exception $e)
    {
        $errors = [];
        $message = 'Server Error';
        $parentRender = parent::render($request, $e);
//        return $parentRender;
//        if (env('APP_DEBUG')) {
//            return $parentRender;
//        }

        // if parent returns a JsonResponse
        // for example in case of a ValidationException
        if ($parentRender instanceof JsonResponse)
        {
            $data = $parentRender->getData(true);
            if (isset($data['message'])) {
                $message = $data['message'];
            }
            $statusCode = $parentRender->getStatusCode();
            switch ($statusCode) {
                case 422:
                    $message = 'INVALID_PARAMS';
                    $errors = $data;
                    break;
                case 404:
                    $message = 'ROUTE_NOT_FOUND';
                    $errors = [];
                    break;
                case 405:
                    $message = 'Method Not Allowed';
                    break;
                default:
            }
            return $this->error($message, ['errors' => $errors], $parentRender->getStatusCode());
        }

        return $this->error($e->getMessage(), ['errors' => $errors], $parentRender->status());
    }
}
