<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\UserInterface;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 * @property UserRepository $repository
 */


class UserController extends Controller
{
    private $repository;
    /**
     * UserController constructor.
     *
     * @param UserInterface $userInterface
     */
    public function __construct(UserInterface $userInterface)
    {
        $this->repository = $userInterface;
    }

    /**
     * get list of users
     */
    public function index() {
        /**
         * @OA\Get(
         *   path="/users",
         *   summary="Get Users",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         * @OA\Parameter(
         *   name="name",
         *   description="The User Full Name",
         *   example="Nguyen Van A",
         *   in="query"
         * ),
         * @OA\Parameter(
         *   name="email",
         *   description="Email Address",
         *   example="abc@gmail.com",
         *   in="query"
         * ),
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        /** @var array $searchFields */
        $searchFields = $this->repository->model()::$searchFields;
        $filters = request()->only($searchFields);
        $likeFilers = [];
        foreach ($filters as $filterKey => $filterValue) {
            if($filterValue) {
                $likeFilers[] = [$filterKey, 'like', '%'.$filterValue.'%'];
            }
        }
        $perPage = $this->repository->getPerPage();
        $users = $this->repository->findByFilters($likeFilers)->paginate($perPage);
        return $this->success($users, __('account.get_info_success'));
    }


    /**
     * Create a new user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store() {
        /**
         * @OA\Post(
         *   path="/users",
         *   summary="Add an User",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         *   @OA\RequestBody(
         *       description="User Info",
         *       required=true,
         *       @OA\MediaType(
         *           mediaType="application/json",
         *           @OA\Schema(ref="#/components/schemas/UserCreateReq")
         *       )
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        // if (!$this->hasAdminPermission())
        //     return $this->error(__('app.no_permission'), [], 403);;
        /** @var array $rules */
        $data = $this->validate(request(), $this->repository->model()::$rules);
        if (!isset($data['password']) || !isset($data['password_confirmation']))
            return $this->error(__('account.password_required'));
        $data = $this->repository->create($data);
        return $this->success($data, __('app.update_success'));
    }

    /**
     * Update an user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id) {
        /**
         * @OA\Put(
         *   path="/users/{id}",
         *   summary="Update an User By Admin",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         *   @OA\Parameter(
         *     name="id",
         *     in="path",
         *     description="User ID"
         *   ),
         *   @OA\RequestBody(
         *       description="User Info",
         *       required=true,
         *       @OA\MediaType(
         *           mediaType="application/json",
         *           @OA\Schema(ref="#/components/schemas/UserUpdateReq")
         *       )
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        if (!$this->hasAdminPermission())
            return $this->error(__('app.no_permission'), [], 403);;
        /** @var array $rules */
        $rules = $this->repository->model()::$rules;
        $rules['email'] = $rules['email'] . ',' .$id;
        $data = $this->validate(request(), $rules);
        $data = $this->repository->update($data, $id);
        return $this->success($data, __('app.update_success'));
    }

    /**
     * Self update user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function selfUpdate() {
        /**
         * @OA\Put(
         *   path="/users",
         *   summary="Update User by self",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         *   @OA\RequestBody(
         *       description="User Info",
         *       required=true,
         *       @OA\MediaType(
         *           mediaType="application/json",
         *           @OA\Schema(ref="#/components/schemas/UserUpdateReq")
         *       )
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        $id = Auth::guard('admin')->user()->id;
        /** @var array $rules */
        $rules = $this->repository->model()::$rules;
        unset($rules['email']);
        $data = $this->validate(request(), $rules);
        unset($data['email']);
        $data = $this->repository->update($data, $id);
        return $this->success($data, __('app.update_success'));
    }

    /**
     * Delete an user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {
        /**
         * @OA\Delete(
         *   path="/users/{id}",
         *   summary="Block an User by ID",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         *   @OA\Parameter(
         *     name="id",
         *     in="path",
         *     description="User ID"
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        if (!$this->hasAdminPermission())
            return $this->error(__('app.no_permission'), [], 403);
        $this->repository->delete($id);
        return $this->success(['id' => $id], __('account.block_or_delete_success'));
    }

    /**
     * Get detail an user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) {
        /**
         * @OA\Get(
         *   path="/users/{id}",
         *   summary="Get detail of an User by ID",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         *   @OA\Parameter(
         *     name="id",
         *     in="path",
         *     description="User ID"
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        $user = $this->repository->find($id);
        return $this->success($user, __('account.get_info_success'));
    }
    /**
     * Self change password
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function selfPassword() {
        /**
         * @OA\Post(
         *   path="/users/password",
         *   summary="Change password by User",
         *   security={{ "apiAuth": {} }},
         *   tags={"User"},
         *   @OA\Parameter(
         *     name="old_password",
         *     in="query",
         *     description="Old Password",
         *     required=True
         *   ),
         *   @OA\Parameter(
         *     name="password",
         *     in="query",
         *     description="New Password",
         *     required=True
         *   ),
         *   @OA\Parameter(
         *     name="password_confirmation",
         *     in="query",
         *     description="Password confirm",
         *     required=True
         *   ),
         *   @OA\Response(
         *     response=401,
         *     @OA\JsonContent(ref="#/components/schemas/JsonResponse"),
         *     description="Json response"
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="an ""unexpected"" error"
         *   )
         * )
         */
        $id = Auth::guard('admin')->user()->id;
        $data = request();
        /** @var array $rules */
        if(!isset($data['old_password'])){
            return $this->error("Old password required");
        }
        if (!isset($data['password']) || !isset($data['password_confirmation'])){
            return $this->error(__('account.password_required'));
        }
        if ($data['password'] != $data['password_confirmation']){
            return $this->error(__('account.password_not_match'));
        }
        $user = Auth::guard('admin')->user();
        if(!Hash::check($data['old_password'], $user->password)){
            return $this->error(__('auth.password_not_correct'));
        }
        $data = $this->repository->update(['password'=>$data['password']], $id);
        return $this->success(['id' => $id], "Change password success. You must login again");
    }
    public function hasAdminPermission()
    {
        return true;
    }

}
