<?php
$cms_scope_define = 'cms-scopes';
$app_scope_define = 'app-scopes';

return [
    'cms_scope_define' => $cms_scope_define,
    'app_scope_define' => $app_scope_define,
    'scopes' => [
        $app_scope_define => 'Accounts Access Client',
        $cms_scope_define => 'Admin Access Token',
    ]
];
