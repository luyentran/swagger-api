<?php
use App\Models\Account;
use Illuminate\Support\Str;

if (!function_exists('request')) {
    /**
     * Get an instance of the current request or an input item from the request.
     *
     * @param  array|string|null $key
     * @param  mixed $default
     * @return \Illuminate\Http\Request|string|array
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        if (is_array($key)) {
            return app('request')->only($key);
        }

        $value = app('request')->__get($key);

        return is_null($value) ? value($default) : $value;
    }
}

if (!function_exists('phoneRegex')) {
    function phoneRegex()
    {
        $regex = config('site.regex.phone');
        return "regex:$regex";
    }
}

if (!function_exists('unique_code')) {
    /**
     * render code.
     *
     * @return string
     */
    function unique_code()
    {
        $statement = DB::select("show table status like 'accounts'");
        $last_id = $statement[0]->Auto_increment * 3;
        $max = Account::CODE_MAX_LENGTH - Account::CODE_PREFIX_LENGTH;
        $factor = count(Account::CODE_CHARTERS);
        $result = [];
        for ($start = $max - 1; $start > 0; $start--) {
            $m = $factor ** $start;
            $charter = Account::CODE_CHARTERS[$last_id / $m];
            $last_id = $last_id % $m;
            $result[] = $charter;
        }
        $result[] = Account::CODE_CHARTERS[$last_id % $factor];
        return Account::CODE_PREFIX . implode('', $result);
    }
}

if (!function_exists('datetime_db_to_view')) {
    function datetime_db_to_view($datetime, $format = 'H:i:s d/m/Y')
    {
        return \Carbon\Carbon::parse($datetime)->format($format);
    }
}
