<?php

return [
    'defaults' => [
        'guard' => 'admin',
        'passwords' => 'users',
    ],

    'guards' => [
        'admin' => [
            'driver' => 'passport',
            'provider' => 'admins',
        ],
        'user' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => \App\Models\Account::class
        ],
        'admins' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],
    ]
];
