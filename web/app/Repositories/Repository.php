<?php

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class Repository
 * @package App\Repositories
 * @property Model|\Illuminate\Database\Eloquent\Builder $model
 */
abstract class Repository extends BaseRepository
{

    /**
     * @param array $where
     * @param array $columns
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function firstWhere(array $where, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($where);

        $model = $this->model->first($columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * @return \App\Services\Paginator
     */
    public function getPaginator() {
        if (!app()->bound('paginator')) {
            app()->singleton('paginator', \App\Services\Paginator::class);
        }
        return app('paginator');
    }

    public function getPerPage() {
        return $this->getPaginator()->getPerPage();
    }

    public function findByFilters($filters, $query = null)
    {
        $result = $this->model;
        foreach ($filters as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $result = $result->where($field, $condition, $val);
            } else {
                $result = $result->where($field, '=', $value);
            }
        }
        return $result;
    }
}
