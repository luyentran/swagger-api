{
    "openapi": "3.0.0",
    "info": {
        "title": "The Notification System API",
        "version": "1.0.0"
    },
    "paths": {
        "/app/login": {
            "post": {
                "tags": [
                    "Auth"
                ],
                "summary": "Login into system",
                "parameters": [
                    {
                        "name": "email",
                        "in": "query",
                        "description": "Email of account"
                    },
                    {
                        "name": "password",
                        "in": "query",
                        "description": "Password",
                        "required": true
                    }
                ],
                "responses": {
                    "401": {
                        "description": "Json response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/JsonResponse"
                                }
                            }
                        }
                    },
                    "default": {
                        "description": "an \"unexpected\" error"
                    }
                }
            }
        },
        "/app/user": {
            "get": {
                "tags": [
                    "Auth"
                ],
                "summary": "User info, that is logging in system",
                "responses": {
                    "401": {
                        "description": "json response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/JsonResponse"
                                }
                            }
                        }
                    },
                    "default": {
                        "description": "an \"unexpected\" error"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            }
        },
        "/app/logout": {
            "get": {
                "tags": [
                    "Auth"
                ],
                "summary": "Logout",
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            }
        },
        "/users": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "Get Users",
                "parameters": [
                    {
                        "name": "name",
                        "in": "query",
                        "description": "The User Full Name",
                        "example": "Nguyen Van A"
                    },
                    {
                        "name": "email",
                        "in": "query",
                        "description": "Email Address",
                        "example": "abc@gmail.com"
                    }
                ],
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            },
            "put": {
                "tags": [
                    "User"
                ],
                "summary": "Update User by self",
                "requestBody": {
                    "description": "User Info",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/UserUpdateReq"
                            }
                        }
                    }
                },
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            },
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "Add an User",
                "requestBody": {
                    "description": "User Info",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/UserCreateReq"
                            }
                        }
                    }
                },
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            }
        },
        "/users/{id}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "Get detail of an User by ID",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "User ID"
                    }
                ],
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            },
            "put": {
                "tags": [
                    "User"
                ],
                "summary": "Update an User By Admin",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "User ID"
                    }
                ],
                "requestBody": {
                    "description": "User Info",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/UserUpdateReq"
                            }
                        }
                    }
                },
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            },
            "delete": {
                "tags": [
                    "User"
                ],
                "summary": "Block an User by ID",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "User ID"
                    }
                ],
                "responses": {
                    "default": {
                        "description": "success"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            }
        },
        "/users/password": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "Change password by User",
                "parameters": [
                    {
                        "name": "old_password",
                        "in": "query",
                        "description": "Old Password",
                        "required": true
                    },
                    {
                        "name": "password",
                        "in": "query",
                        "description": "New Password",
                        "required": true
                    },
                    {
                        "name": "password_confirmation",
                        "in": "query",
                        "description": "Password confirm",
                        "required": true
                    }
                ],
                "responses": {
                    "401": {
                        "description": "Json response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/JsonResponse"
                                }
                            }
                        }
                    },
                    "default": {
                        "description": "an \"unexpected\" error"
                    }
                },
                "security": [
                    {
                        "apiAuth": []
                    }
                ]
            }
        }
    },
    "components": {
        "schemas": {
            "UserBase": {
                "description": "UserBase",
                "properties": {
                    "name": {
                        "description": "The User Name",
                        "type": "string",
                        "example": "Nguyen Van A"
                    },
                    "email": {
                        "description": "Email Address",
                        "type": "string",
                        "example": "abc@gmail.com"
                    }
                },
                "type": "object"
            },
            "UserResponse": {
                "description": "UserResponse",
                "allOf": [
                    {
                        "$ref": "#/components/schemas/UserBase"
                    }
                ]
            },
            "UserCreateReq": {
                "description": "UserCreateReq",
                "type": "object",
                "allOf": [
                    {
                        "properties": {
                            "password": {
                                "description": "Password",
                                "type": "string",
                                "example": "123456"
                            },
                            "password_confirmation": {
                                "description": "Password",
                                "type": "string",
                                "example": "123456"
                            }
                        }
                    },
                    {
                        "$ref": "#/components/schemas/UserBase"
                    }
                ]
            },
            "UserUpdateReq": {
                "description": "UserUpdateReq",
                "type": "object",
                "allOf": [
                    {
                        "properties": {
                            "password": {
                                "description": "Password",
                                "type": "string",
                                "example": "123456"
                            },
                            "password_confirmation": {
                                "description": "Password",
                                "type": "string",
                                "example": "123456"
                            }
                        }
                    },
                    {
                        "$ref": "#/components/schemas/UserBase"
                    }
                ]
            },
            "JsonResponse": {
                "properties": {
                    "status": {
                        "description": "Status message, success or error",
                        "type": "string",
                        "example": "400"
                    },
                    "message": {
                        "description": "Detail message",
                        "type": "string",
                        "example": "message content"
                    },
                    "data": {
                        "description": "Data of response",
                        "type": "object"
                    }
                },
                "type": "object"
            }
        },
        "securitySchemes": {
            "apiAuth": {
                "type": "http",
                "description": "Login with email and password to get the authentication token",
                "name": "Token based Based",
                "in": "header",
                "bearerFormat": "JWT",
                "scheme": "bearer"
            }
        }
    }
}