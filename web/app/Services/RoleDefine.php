<?php
namespace App\Services;

class RoleDefine
{
    public static function getMasterAdmin() {
        return config('access.users.master_admin_role');
    }

    public static function getAdmin() {
        return config('access.users.admin_role');
    }

    public static function getManager() {
        return config('access.users.manager_role');
    }

    public static function getAccountant() {
        return config('access.users.accountant_role');
    }

    public static function getStoreKeeper() {
        return config('access.users.storekeeper_role');
    }

    public static function getBusiness() {
        return config('access.users.business_role');
    }

    public static function getDefault() {
        return config('access.users.default_role');
    }

    public static function getRoles() {
        return [
            self::getMasterAdmin(),
            self::getAdmin(),
            self::getManager(),
            self::getAccountant(),
            self::getStoreKeeper(),
            self::getBusiness(),
            self::getDefault()
        ];
    }
}
