<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Exceptions\MissingScopeException;

class CheckScopes extends \Laravel\Passport\Http\Middleware\CheckScopes
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param mixed ...$scopes
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\AuthenticationException|\Laravel\Passport\Exceptions\MissingScopeException
     */
    public function handle($request, $next, ...$scopes)
    {
        $scopeFile = config('scopes');
        if (in_array($scopeFile['app_scope_define'], $scopes)) {
            Auth::setDefaultDriver('user');
            if($request->user()->token_access != request()->bearerToken()) {
                return abort(401, 'Unauthorized');
            }
        }

        if (!$request->user() || !$request->user()->token()) {
            throw new AuthenticationException;
        }

        foreach ($scopes as $scope) {
            if (!$request->user()->tokenCan($scope)) {
                throw new MissingScopeException($scope);
            }
        }

        return $next($request);
    }

}
