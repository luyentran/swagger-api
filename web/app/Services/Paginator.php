<?php

namespace App\Services;


class Paginator
{

    public $per_page = 30;
    public $allow_per_page = [10, 20, 30, 50, 100];

    public function getPerPage() {
        return $this->per_page;
    }

    public function checkAllowPerPage($number) {
        return in_array($number, $this->allow_per_page);
    }

    public function setPerPage($number) {
        if ($this->checkAllowPerPage($number)) {
            $this->per_page = $number;
        }
    }
}
