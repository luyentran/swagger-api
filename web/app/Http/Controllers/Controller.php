<?php

namespace App\Http\Controllers;

use App\Traits\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
	/**
     * @OA\Info(
     *   title="The Notification System API",
     *   version="1.0.0",
     * )
     * @OA\SecurityScheme(
     *     type="http",
     *     description="Login with email and password to get the authentication token",
     *     name="Token based Based",
     *     in="header",
     *     scheme="bearer",
     *     bearerFormat="JWT",
     *     securityScheme="apiAuth",
     * )
     */
    use JsonResponse;
}
