<?php

/**
 * User Base
 *
 * @OA\Schema(
 *   schema="UserBase",
 *   description="UserBase",
 *   @OA\Property(
 *      property="name",
 *      type="string",
 *      description="The User Name",
 *      example="Nguyen Van A"
 *   ),
 *   @OA\Property(
 *      property="email",
 *      type="string",
 *      description="Email Address",
 *      example="abc@gmail.com"
 *   )
 * )
 *
 */

/**
 * User Response
 *
 * @OA\Schema(
 *   schema="UserResponse",
 *   description="UserResponse",
 *   allOf={
 *     @OA\Schema(ref="#/components/schemas/UserBase"),
 *   }
 * )
 *
 */

/**
 * User create request
 *
 * @OA\Schema(
 *   schema="UserCreateReq",
 *   description="UserCreateReq",
 *   allOf={
 *     @OA\Schema(ref="#/components/schemas/UserBase"),
 *   },
 *   @OA\Property(
 *      property="password",
 *      type="string",
 *      description="Password",
 *      example="123456"
 *   ),
 *   @OA\Property(
 *      property="password_confirmation",
 *      type="string",
 *      description="Password",
 *      example="123456"
 *   )
 * )
 *
 */

/**
 * User update request
 *
 * @OA\Schema(
 *   schema="UserUpdateReq",
 *   description="UserUpdateReq",
 *   allOf={
 *     @OA\Schema(ref="#/components/schemas/UserBase"),
 *   },
 *   @OA\Property(
 *      property="password",
 *      type="string",
 *      description="Password",
 *      example="123456"
 *   ),
 *   @OA\Property(
 *      property="password_confirmation",
 *      type="string",
 *      description="Password",
 *      example="123456"
 *   )
 * )
 *
 */
