<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
/**
 * Class Group
 * @package App\Models
 */
class Shorten extends Model
{
    protected $table = 'shorten';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'link',
        'alias',
    ];
    
    public $timestamps = true;
   
    public static $rules = [
        'link' => 'required',
        'alias' => 'required',
    ];

    public static $rulesPerPage = [
        'per_page'=>'regex:/^[0-9]+$/',
    ];
    
    public static $searchFields = [
      'link',
      'alias',
    ];
}
