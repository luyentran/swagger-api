<?php
namespace App\Http\Controllers;


use App\Models\User;
use App\Repositories\Contracts\UserInterface;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers
 * @property UserRepository $repository
 */
class AuthController extends Controller
{
    private $repository;

    public function __construct(UserInterface $user)
    {
        $this->repository = $user;
    }

    /**
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function login(){
        /**
         * @OA\Post(
         *   path="/app/login",
         *   summary="Login into system",
         *   tags={"Auth"},
         *   @OA\Parameter(
         *     name="email",
         *     in="query",
         *     description="Email of account"
         *   ),
         *   @OA\Parameter(
         *     name="password",
         *     in="query",
         *     description="Password",
         *     required=True
         *   ),
         *   @OA\Response(
         *     response=401,
         *     @OA\JsonContent(ref="#/components/schemas/JsonResponse"),
         *     description="Json response"
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="an ""unexpected"" error"
         *   )
         * )
         */
        $data = $this->validate(request(),[
            'email' => 'required|email|min:6|max:60',
            'password' => 'required|min:6|max:32',
            'remember_me' => 'sometimes|boolean'
        ]);

        $rememberMe = false;
        if (isset($data['remember_me'])) {
            $rememberMe = (bool) $data['remember_me'];
            unset($data['remember_me']);
        }

        /** @var User $user */
        $user = $this->repository->firstWhere(['email' => $data['email']]);

        if (!$user) {
            abort(400, __('auth.email_not_exist'));
        }

        if(!Hash::check($data['password'], $user->password)){
            abort(400, __('auth.password_not_correct'));
        }

        $cmsScope = config('scopes.cms_scope_define');
        $tokenResult = $user->createToken(config('scopes.scopes.' . $cmsScope), [$cmsScope]);
        /** @var \Laravel\Passport\Token $token */
        $token = $tokenResult->token;

        if ($rememberMe) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return $this->success([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], __('auth.success'));
    }

    /**
     * Get the authenticated User
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        /**
         * @OA\Get(
         *   path="/app/user",
         *   summary="User info, that is logging in system",
         *   security={{ "apiAuth": {} }},
         *   tags={"Auth"},
         *   @OA\Response(
         *     response=401,
         *     @OA\JsonContent(ref="#/components/schemas/JsonResponse"),
         *     description="json response"
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="an ""unexpected"" error"
         *   )
         * )
         */
        return $this->success( Auth::guard('admin')->user());
    }

    public function logout() {
        /**
         * @OA\Get(
         *   path="/app/logout",
         *   summary="Logout",
         *   security={{ "apiAuth": {} }},
         *   tags={"Auth"},
         *   @OA\Response(
         *     response="default",
         *     description="success"
         *   )
         * )
         */
        /** @var User $user */
        $user = Auth::guard('admin')->user();
        $token = $user->token();
        $token->revoke();
        return $this->success(null, __('auth.logout'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function emailResetPassword(){
        /**
         * @OA\Post(
         *   path="/forgot-password,
         *   summary="Forgot password",
         *   tags={"Auth"},
         *   @OA\Parameter(
         *     name="email",
         *     in="query",
         *     description="Email of account"
         *   ),
         *   @OA\Response(
         *     response=401,
         *     @OA\JsonContent(ref="#/components/schemas/JsonResponse"),
         *     description="Json response"
         *   ),
         *   @OA\Response(
         *     response="default",
         *     description="an ""unexpected"" error"
         *   )
         * )
         */
        $data = $this->validate(request(),[
            'email' => 'required|email|min:6|max:60'
        ]);
        /** @var User $user */
        $user = $this->repository->firstWhere(['email' => $data['email']]);
        if (!$user) {
            return $this->error(__('auth.email_not_exist'), [], 400);
        }
        // $cmsScope = config('scopes.cms_scope_define');
        // $tokenResult = $user->createToken(config('scopes.scopes.' . $cmsScope), [$cmsScope]);
        /** @var \Laravel\Passport\Token $token */
        // $token = $tokenResult->token;
    }

}
