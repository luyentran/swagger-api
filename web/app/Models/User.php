<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

/**
 * Class User
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, HasRoles, Authenticatable, Authorizable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

//    protected $with = ['role'];

    public static $rules = [
        'name'  => 'required|min:4|max:60',
        'email' => 'required|min:6|max:60|email|unique:users,email',
        'password' => 'sometimes|min:6|max:60|required_with:password_confirmation|same:password_confirmation',
        'password_confirmation' => 'sometimes|min:6|max:60'
    ];

    public static $searchFields = [
        'name',
        'email'
    ];

}
