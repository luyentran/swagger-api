<?php

namespace App\Repositories;


use App\Models\User;
use App\Repositories\Contracts\UserInterface;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository implements UserInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function create(array $attributes)
    {
        $attributes['password'] = Hash::make($attributes['password']);
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        if (isset($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
        return parent::update($attributes, $id);
    }

}
