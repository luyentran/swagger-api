<?php

namespace App\Repositories;


use App\Models\Shorten;
use App\Repositories\Contracts\ShortenInterface;
use Illuminate\Support\Facades\Hash;

class ShortenRepository extends Repository implements ShortenInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Shorten::class;
    }

    public function create(array $attributes)
    {
        return parent::create($attributes);
    }
}
