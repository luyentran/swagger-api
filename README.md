api source
===============

$ docker-compose up -d --build

$ docker exec -it api_web /bin/bash

$ /bin/bash initialize.sh

#config env
#email
MAIL_MAILER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=luyentran@oven.bz
MAIL_PASSWORD=null
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS="luyentran@oven.bz"
MAIL_FROM_NAME="${APP_NAME}"

#database
DB_CONNECTION=pgsql
DB_HOST=api_db
DB_PORT=5432
DB_DATABASE=apidb
DB_USERNAME=postgres
DB_PASSWORD=postgres

#app install

composer install

php artisan key:generate

php artisan migrate

php artisan db:seed

php artisan swagger-lume:generate

php artisan swagger-lume:publish

php artisan swagger-lume:publish-config

php artisan swagger-lume:publish-view

http://localhost/api/documentation

# start docker
docker-compose up


# login
email: admin@gmail.com
pass: 123456
